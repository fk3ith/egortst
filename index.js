
const express = require('express')
const faker = require('faker')
var bodyParser = require('body-parser');
const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
const port = 3000

app.get('/', (req, res) => {
  res.set('Content-Type', 'application/json')
  res.send(getTestJson())
})

app.get('/authors', (req, res) => {
  let fakeAuthors = createAuthors(10);
  res.set('Content-Type', 'application/json')
  res.send(fakeAuthors)
})

app.get('/books', (req, res) => {
  let fakeBooks = createBooks(10);
  res.set('Content-Type', 'application/json')
  res.send(fakeBooks)
})

app.post('/order', (req, res) => {
  order = placeOrder(req.body.name, req.body.price)
  res.set('Content-Type', 'application/json')
  res.send(order)
})

app.put('/order/:orderId', (req, res) => {
  orderId = req.params.orderId
  order = payOrder(orderId)
  res.set('Content-Type', 'application/json')
  res.send(order)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

function getTestJson() {
  var testObj = new Object()
  testObj.Who = 'Ты'
  testObj.Which = 'пидор'

  return testObj
}

function getAuthors() {
  var author = new Object()

}


// createAuthor returns random author info
const createAuthor = () => {
  return {
    id: getRandomInt(1, 9999),
    name: faker.name.findName(),
    email: faker.internet.email(),
    address: faker.address.streetAddress(),
    bio: faker.lorem.sentence(),
    image: faker.image.avatar(),
  };
};

const createAuthors = (numUsers = 5) => {
    return Array.from({length: numUsers}, createAuthor)
}

// createBook returns random book info
const createBook = () => {
  return {
    id: getRandomInt(1, 9999),
    name: faker.commerce.product(),
    desc: faker.commerce.productDescription(),
    image: faker.image.abstract(),
    author: createAuthor(),
    price: faker.commerce.price(),
  };
};

const createBooks = (numBooks = 10) => {
    return Array.from({length: numBooks}, createBook)
}

// getRandomInt generating random id (int)
function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

// placeOrder creates an order
function placeOrder(bookName, price) {
  var order = new Object()
  order.id = getRandomInt(1, 100)
  order.book = bookName
  order.total = price
  order.status = 'created'
  return order
}

// payOrder mark order as paid
function payOrder(orderId) {
  var order = new Object()
  order.id = orderId
  order.status = 'paid'
  return order
}